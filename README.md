STM32F373CxT6 development board (kicad)
----
![stm32f373cxt6](stm32f373cxt6.png).

This development board is designed around the new STM32F373CxT6 mcu in kicad.
You can find more info for the mcu [here](https://www.st.com/en/microcontrollers/stm32f373cb.html)
and the full mcu manual [here](https://www.st.com/resource/en/datasheet/dm00046749.pdf).

Some key features are:

* ARM Cortex-M4 @ 72MHz, w/ DSP, FPU and MPU
* 64KB flash
* 32KB SRAM
* 12-ch DMA
* 12-bit ADC aup to 16-ch
* 3x 16-bit Sigma Delta ADCs
* 3x 12-bit DAC
* 2x rail-to-rail analog comparators
* 17 timers (3x 32-bit, 6x 16-bit ...)
* 2x I2C, 3x USART, 3x SPI, USB, CEC, Serial-Wire
* 48 pin LQFP

> This board is yet un-tested, so it may have errors. Check it
your self before build it.


## Author
Dimitris Tassopoulos <dimtass@gmail.com>

## Kicad version
5.0.2-bee76a0~70~ubuntu18.04.1, release build
